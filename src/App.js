import logo from './logo.svg';
import './App.css';
import Navbarr from './components/Navbarr';
import Formm from './components/Form'
import Table from './components/Table'

function App() {
  return (
    <div className="App">
        <Navbarr/>
        <Formm/>
        <Table/>
    </div>
  );
}

export default App;

